class Animal:
    def __init__(self, p):
        self.peso = p

class Mamifero(Animal):
    def __init__(self, p, c):
        super().__init__(self, p, c):
        self.crias = c

p1 = Animal(30)
p2 = Mamifero(25,9)
