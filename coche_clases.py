class Coche:
    def __init__(self, color, marca, modelo, matricula, velocidad):
        self.color = color
        self.marca = marca
        self.modelo = modelo
        self.matricula = matricula
        self.velocidad = velocidad
    
    def aceleracion(self):
        return self.velocidad * 2
    
    def freno(self):
        return self.velocidad / 2

    def __str__(self):
        return('La aceleración del coche es {aceleracion} m/s² y el freno de {freno} m/s²'.format(aceleracion= self.aceleracion(), freno = self.freno ()))

coche1 = Coche('blanco', 'ferrari', 'ferrari 488', '3579DFK', 30)
print(coche1)
