class Cuadrado:
    """Un ejemplo de clase para los cuadrados"""
    def __init__(self, lado): #qué tiene
        self.lado = lado
        #self.area = self.lado**2 

    def CalculoPerimetro(self):
        return self.lado * 4

    def CalculoArea(self):
        return self.lado * self.lado #area(para el caso de self.area. Para que funcione es necesario meterle un self)

cuadrado1 = Cuadrado(2)
cuadrado2 = Cuadrado(3)

print('El perímetro del cuadrado de lado 2 es: ',cuadrado1.CalculoPerimetro()) #llamo a un metodo, no a una funcion 
print('El perímetro del cuadrado de lado 3 es: ',cuadrado2.CalculoPerimetro()) 
print('El perímetro del cuadrado de lado 2 es: ',cuadrado1.CalculoArea()) 
print('El perímetro del cuadrado de lado 3 es: ',cuadrado2.CalculoArea()) 