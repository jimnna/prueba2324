class Empleado:
    """Un ejemplo de clase para Empleados"""
    def __init__(self, n, s):
        self.nombre = n
        self.nomina = s
    
    def calculo_impuestos(self):
        return self.nomina * 0.30
    
    def __str__(self):
        return "El empleado {name} debe pagar {tax:.2f}".format(name=self.nombre, tax=self.calculo_impuestos())

empleadoPepe = Empleado("Pepe", 20000)
empleadaAna = Empleado("Ana", 30000)

total = empleadoPepe.calculo_impuestos() + empleadaAna.calculo_impuestos()

print(empleadoPepe)
print(empleadaAna)
print('Los impuestos a pagar en total son {:.2f} euros'.format(total))