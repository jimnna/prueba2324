from coche_clases import Coche
import unittest

class CocheTestClase(unittest.TestCase):
    def test_aceleracion(self):
        coche1 = Coche('blanco', 'ferrari', 'ferrari 488', '3579DFK', 30)
        aceleracion = coche1.aceleracion()
        self.assertEqual (aceleracion, 60)

    def test_freno(self):
        coche1 =Coche('blanco', 'ferrari', 'ferrari 488', '3579DFK', 30)
        frenar = coche1.freno()
        self.assertEqual (frenar, 15)


unittest.main()
        
