class Rectangulo:
    def __init__(self, b, h):
        self.base = b
        self.altura = h


    def CalculoPerimetro(self):
        return self.base*2 + self.altura*2


    def CalculoArea(self):
        return(self.base*self.altura)


    def __str__(self):
        return('El rectángulo de base {base} y altura {altura} tiene un área de {area} y un perímetro de {perímetro}'.format(base=self.base, altura=self.altura, area=self.CalculoArea(), perímetro=self.CalculoPerimetro()))

rectangulo1 = Rectangulo(2,3)
rectangulo2 = Rectangulo(4,5)
print(rectangulo1)
print(rectangulo2)
