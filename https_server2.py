#!/usr/bin/python #para funcionar tengo que ejecutar esa línea
from http.server import HTTPServer, BaseHTTPRequestHandler

PORT_NUMBER = 8080

mihtml = """
<!DOCTYPE html>
<html>
    <head>
        <title>Page Title</title>
    </head>
    <body>

        <p>hola bonita!!!! deja de coquetear</p>
        <p><br></p>

    </body>
</html>"""

class miServidor(BaseHTTPRequestHandler):

    # Este metodo gestiona las peticiones GET de HTTP
    def do_GET(self):
        self.send_response(200)
        self.send_header('Content-type','text/html')
        self.end_headers()
        # Nos retorna la respuesta
        self.wfile.write(mihtml.encode("utf-8"))
        return

try:
    server = HTTPServer(('', PORT_NUMBER), miServidor)
    print('Started httpserver on port ' , PORT_NUMBER)

    #Wait forever for incoming http requests
    server.serve_forever()

except KeyboardInterrupt:
    print('Control-C received, shutting down the web server')
    server.socket.close()
