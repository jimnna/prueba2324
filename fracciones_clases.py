class Fraccion:
    def __init__(self, numerador=0.0, denominador=1.0): #para poner un valor por defecto
        self.numerador = numerador
        self.denominador = denominador


    def sumar(izq, dcha): #es una FUNCIÓN
        return Fraccion(izq.numerador*dcha.denominador + dcha.numerador*izq.denominador)


    def suma(self, otro): #es un MÉTODO
        self.numerador = self.numerador * otro.denominador + self.denominador * otro.numerador
        self.denominador = self.denominador * otro.denominador
        return self.numerador, self.denominador

    def resta(self, otro):
        self.numerador = self.numerador * otro.denominador - self.denominador * otro.numerador
        self.denominador = self.denominador * otro.denominador
        return self.numerador, self.denominador

    def division(self, otro):
        self.numerador = self.numerador * otro.denominador
        self.denominador = self.denominador * otro.numerador
        return self.numerador, self.denominador

    def multiplicacion(self, otro):
        self.numerador = self.numerador * otro.numerador
        self.denominador = self.denominador * otro.denominador
        return self.numerador, self.denominador

fraccion1 = Fraccion(2,3)
fraccion2 = Fraccion(1,2)

misuma = fraccion2.suma(fraccion1)
miresta = fraccion2.resta(fraccion1)
midiv = fraccion2.division(fraccion1)
mimul = fraccion2.multiplicacion(fraccion1)


print(misuma.numerador, '/', misuma.denominador)
print(miresta.numerador, '/', miresta.denominador)
print(mimul.numerador, '/', mimul.denominador)
print(midiv.numerador, '/', midiv.denominador)




 

