import rectangulo_clases
import unittest

class RectanguloTestCase(unittest.TestCase):
    def test_perimetro(self):
        rectangulo1 = rectangulo_clases.Rectangulo(2,3)
        perimetro1=rectangulo1.CalculoPerimetro()
        self.assertEqual(perimetro1, 10)

unittest.main()