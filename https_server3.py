#!/usr/bin/python
from http.server import HTTPServer, BaseHTTPRequestHandler

PORT_NUMBER = 21080

class miServidor(BaseHTTPRequestHandler):

    # Este método gestiona las peticiones GET de HTTP
    def do_GET(self):
        self.send_response(200)
        self.send_header('Content-type','text/html')
        self.end_headers()
        # Retorna la respuesta
        with open('index.html', 'r') as f:
            contenido = f.read()
            print(contenido)

        self.wfile.write(contenido.encode('utf-8'))
        return

try:
    server = HTTPServer(('', PORT_NUMBER), miServidor)
    print('Started httpserver on port', PORT_NUMBER)

    # Esperar siempre por las peticiones HTTP entrantes
    server.serve_forever()

except KeyboardInterrupt:
    print('Control-C recibido, cerrando el servidor web')
    server.socket.close()

